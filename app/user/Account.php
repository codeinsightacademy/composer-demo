<?php

namespace App\User;

class Account {
	
	//this is magic method as it will invoked automatically when you create instance of account
	public function __construct() {
		
		echo "I am from User Account" . PHP_EOL;
	}
	
}