<?php

namespace App\Admin;

class Account {
	
	//this is magic method as it will invoked automatically when you create instance of account
	public function __construct() {
		
		echo "I am from Admin Account" . PHP_EOL;
	}
	
}